package com.example.event_bus;

import com.google.common.eventbus.EventBus;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventBusApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventBusApplication.class, args);

        EventBus eventBus = new EventBus();
        EventListener listener = new EventListener();
        eventBus.register(listener);

        CustomEvent customEvent = new CustomEvent("custom event");
        eventBus.post(customEvent);
    }

}
