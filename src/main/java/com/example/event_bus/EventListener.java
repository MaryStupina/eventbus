package com.example.event_bus;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventListener {
    private static int eventsHandled;

    @Subscribe
    public void stringEvent(String event){
        eventsHandled++;
    }

    @Subscribe
    public void someCustomEvent(CustomEvent event){
        eventsHandled++;
        log.info("Had cought CustomEvent: " + eventsHandled);
    }

    @Subscribe
    public void handleDeadEvent(DeadEvent event){
        eventsHandled++;
        log.info("Had cought DeadEvent: " + eventsHandled);
    }
}
